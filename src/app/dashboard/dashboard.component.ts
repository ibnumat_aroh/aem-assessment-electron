import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';
import * as d3 from 'd3';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  displayedColumns = ['firstName', 'lastName', 'username'];
  dataSource = [];
  private svg: any;
  private margin = 50;
  private width = 500 - (this.margin * 2);
  private height = 450 - (this.margin * 2);
  private heightPie = 550 - (this.margin * 2);
  // The radius of the pie chart is half the smallest side
  private radius = Math.min(this.width, this.heightPie) / 2 - this.margin;
  private colors: any;
  private donut = [];
  private bar = [];
  private isLoggedIn = false;
  constructor(private storageService: StorageService, private router: Router, private http:HttpClient) { }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      const headers = { 'Authorization': 'Bearer ' + user }
      this.http.get<any>('http://test-demo.aemenersol.com/api/dashboard', {headers}).subscribe(output => {
        this.donut = output.chartDonut
        this.bar = output.chartBar
        this.dataSource = output.tableUsers
        console.log(output);
        console.log(this.donut);
        console.log(this.bar);
        console.log(this.dataSource);
        this.createBarSvg();
        this.drawBars(this.bar);
        this.createPieSvg();
        this.createColors();
        this.drawChart();
      });
    }
    else {
      alert("Please login first")
      this.router.navigate(['/login']);
    }
  }

  logout(): void {
    this.storageService.clean();
    this.router.navigate(['/login']);
  }

  private createBarSvg(): void {
    this.svg = d3.select("figure#bar")
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }

  private createPieSvg(): void {
    this.svg = d3.select("figure#pie")
    .append("svg")
    .attr("width", this.width)
    .attr("height", this.heightPie)
    .append("g")
    .attr(
      "transform",
      "translate(" + this.width / 2 + "," + this.heightPie / 2 + ")"
    );
}

  private drawBars(data: any[]): void {
    // Create the X-axis band scale
    const x = d3.scaleBand()
    .range([0, this.width])
    .domain(data.map(d => d.name))
    .padding(0.2);
  
    // Draw the X-axis on the DOM
    this.svg.append("g")
    .attr("transform", "translate(0," + this.height + ")")
    .call(d3.axisBottom(x))
    .selectAll("text")
    .attr("transform", "translate(-10,0)rotate(-45)")
    .style("text-anchor", "end");
  
    // Create the Y-axis band scale
    const y = d3.scaleLinear()
    .domain([0, 100])
    .range([this.height, 0]);
  
    // Draw the Y-axis on the DOM
    this.svg.append("g")
    .call(d3.axisLeft(y));
  
    // Create and fill the bars
    this.svg.selectAll("bars")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", (d: any) => x(d.name))
    .attr("y", (d: any) => y(d.value))
    .attr("width", x.bandwidth())
    .attr("height", (d: any) => this.height - y(d.value))
    .attr("fill", "#d04a35");
  }

  private drawChart(): void {
    // Compute the position of each group on the pie:
    const pie = d3.pie<any>().value((d: any) => Number(d.value));
  
    // Build the pie chart
    this.svg
    .selectAll('pieces')
    .data(pie(this.donut))
    .enter()
    .append('path')
    .attr('d', d3.arc()
      .innerRadius(75)
      .outerRadius(this.radius)
    )
    .attr('fill', (d: any, i: any) => (this.colors(i)))
    .attr("stroke", "#121926")
    .style("stroke-width", "1px");
  
    // Add labels
    const labelLocation = d3.arc()
    .innerRadius(100)
    .outerRadius(this.radius);
    this.svg
    .selectAll('pieces')
    .data(pie(this.donut))
    .enter()
    .append('text')
    .text((d: any)=> d.data.name)
    .attr("transform", (d: any) => "translate(" + labelLocation.centroid(d) + ")")
    .style("text-anchor", "middle")
    .style("font-size", 15);
  }

  private createColors(): void {
    this.colors = d3.scaleOrdinal()
    .domain(this.donut.map(d => d['value']))
    .range(["#c7d3ec", "#a5b8db", "#879cc4", "#677795"]);
}
}
