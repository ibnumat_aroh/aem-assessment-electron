import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root', 
})

export class AuthService {

  constructor(private http:HttpClient) { }

  public login(username: string, password: string): Observable<any> {
    return this.http.post<any>(
      'http://test-demo.aemenersol.com/api/account/login', 
      {username, password},
      httpOptions
    );
  }
}
