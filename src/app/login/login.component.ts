import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  user = new User

  constructor(private authService: AuthService, private router:Router, private storageService: StorageService) {}

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
    }
  }

  login(username: string, password: string) {
    // const router = new Router();
    const helper = new JwtHelperService();

    this.authService.login(username, password).subscribe({
      next: token => {
        alert("Login Successful")
        const data = helper.decodeToken(token);
        this.storageService.saveUser(token);
        console.log(token)
        console.log(data)
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = data['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
        this.router.navigate(['dashboard']); 
      },
      error: err => {
        // this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }});
  }

}
